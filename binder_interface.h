// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FIREWALLD_BINDER_INTERFACE_H_
#define FIREWALLD_BINDER_INTERFACE_H_

#include <base/macros.h>

#include "android/firewalld/BnFirewall.h"

namespace firewalld {

class IpTables;
class BinderInterface : public android::firewalld::BnFirewall {
 public:
  explicit BinderInterface(IpTables* service);
  virtual ~BinderInterface() = default;

  android::binder::Status PunchTcpHole(int32_t port, const std::string& iface)
      override;
  android::binder::Status PunchUdpHole(int32_t port, const std::string& iface)
      override;
  android::binder::Status PlugTcpHole(int32_t port, const std::string& iface)
      override;
  android::binder::Status PlugUdpHole(int32_t port, const std::string& iface)
      override;
  android::binder::Status RequestVpnSetup(
      const std::vector<std::string>& usernames,
      const std::string& iface) override;
  android::binder::Status RemoveVpnSetup(
      const std::vector<std::string>& usernames,
      const std::string& iface) override;

 private:
  IpTables* service_;

  DISALLOW_COPY_AND_ASSIGN(BinderInterface);
};

}  // namespace firewalld

#endif /* FIREWALLD_BINDER_INTERFACE_H_ */
